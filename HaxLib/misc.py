from attrdict import AttrDict
from json import loads
from asyncio import sleep

def AttrLoad(data):
    if isinstance(data, dict):
        return AttrDict(data)
    elif isinstance(data, str):
        return AttrDict(loads(data))
    else:
        print(f"type {type(data)} is not a valid data type for this operation")

async def ratelimit(d, v, currentv):
    if v <= currentv:
        await sleep(d)
        currentv = 0
    return currentv

def ratelimits(_headers):
    headers={}
    for x in _headers:
        headers[x] = _headers[x]
        ratelimits={}
        for i in headers:
            if 'ratelimit' in i.lower():
                ratelimits[i]=headers[i]
    del(_headers, i, headers)
    return AttrLoad(ratelimits)