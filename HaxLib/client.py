import httpx, json, asyncio
from .misc import AttrLoad, ratelimits

api = 'https://discordapp.com/api/v6'

class Client(object):
    def __init__(self, token):
        self.xclient = httpx.AsyncClient()
        self.token = token
        self.headers = {"Authorization":f"Bot {self.token}"}

    async def send_msg(self, channel_id, msg:str=None,embed=None):
        url = f'{api}/channels/{str(channel_id)}/messages'
        payload = {"content":msg, "embed":embed}
        res = await self.xclient.post(url=url, headers=self.headers, json=payload)
        ratelimitinfo=ratelimits(res.headers)
        return AttrLoad(res.text)
        
    async def edit_msg(self, message, msg:str=None, embed=None):
        payload = {"content":msg, "embed":embed}
        res = await self.xclient.post(url=url, headers=self.headers, json=payload)
        return AttrLoad(res.text)

    async def get_msg(self, channel_id, message_id):
        url = f'{api}/channels/{str(channel_id)}/messages/{str(message_id)}'
        res = await self.xclient.get(url=url, headers=self.headers)
        return AttrLoad(res.text)

    async def react(self, message, emoji):
        url = f'{api}/channels/{message.id}/messages/{message.id}/reactions/{emoji}/@me'
        res = await self.xclient.put(url=url, headers=self.headers)
        return AttrLoad(res.text)

    async def create_inv(self, channel_id, max_uses=0, max_age=0):
        payload = {
          'max_uses':max_uses,
          'max_age':max_age
          }
        url = f'{api}/channels/{str(channel_id)}/invites'
        res = await self.xclient.post(url=url, headers=self.headers, json=payload)
        return f'https://discord.gg/{json.loads(res.text)["code"]}'

    async def get_channel(self, channel_id):
        url = f"{api}/channels/{channel_id}"
        res = await self.xclient.get(url=url, headers=self.headers)
        return AttrLoad(res.text)

    async def start_bot(self):
        pass
