# About
HaxLib is a library for making Discord bots that will have a Ruby and Python version, so it's easier to port bots between the two. 

# Why use HaxLib
It's a project created for fun - using it in production isn't recommended.
Suggestions instead: 
- [Hata](https://github.com/HuyaneMatsu/hata) for Python

- [Discord.py](https://discordpy.readthedocs.io/en/latest/) for Python

- [Discord.rb](https://github.com/discordrb/discordrb) for Ruby

# Support
You can get support at [our support server](https://discord.gg/dQuv5vT)

# Contributing
Feel free to contribute! In fact, we'd love it! We are just doing this as a side project and having more people join us would be fun!